import { Navigation } from "react-native-navigation";

import { registerScreens } from "./app/config/RegisterScreens";

registerScreens(); // this is where you register all of your app's screens

const navigatorStyle = {
	navBarBackgroundColor: "#1E1C1A",
	navBarTextColor: "#FFD500",
	navBarTextFontFamily: "Oswald",
	statusBarTextColorScheme: "light",
	navBarButtonColor: "#FFD500",
};

Navigation.startSingleScreenApp({
	screen: {
		screen: "HomePage", // unique ID registered with Navigation.registerScreen
		title: "", // title of the screen as appears in the nav bar (optional)
		navigatorStyle,
		leftButtons: [
			{
				id: "sideMenu"
			}
		]
	},
	drawer: { // optional, add this if you want a side menu drawer in your app
		left: { // optional, define if you want a drawer from the left
			screen: "DrawerMenu", // unique ID registered with Navigation.registerScreen
		}
	  },
	passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
	animationType: "none",
	portraitOnlyMode: true
});



