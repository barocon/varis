import { StyleSheet, PixelRatio } from "react-native";

var yellow = "#FFD500";
var baseFontSize = 6 * PixelRatio.get();
var font = "Oswald"

var styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#26251D",
		alignItems: "center",
		justifyContent: "center",
	},
	actionFrame: {
		alignSelf: "stretch",
		backgroundColor: "#484739",
		justifyContent: "center",
		padding: "4%",
		margin: "4%",
		flex: 1,
		minHeight: "95%"
	},
	baseText: {
		color: "#fff",
		fontSize: baseFontSize,
		marginTop: 1
	},
	H2: {
		color: yellow,
		fontSize: baseFontSize+2,
		marginTop: "2%",
		marginBottom: 2
	},
	H1: {
		fontFamily: font,
		color: "#fff",
		fontSize: baseFontSize+8,
		marginTop: 1
	},
	button: {
		padding:"1.5%",
		height: 20 * PixelRatio.get(),
		width: 95 * PixelRatio.get(),
		overflow:"hidden",
		borderRadius:7,
		backgroundColor: yellow,
		alignItems: "center",
		justifyContent: "center",
		marginTop: "1.5%",
		paddingBottom: "1.5%",
	},
	buttonText: {
		fontSize: baseFontSize+8,
		color: "black",
		fontFamily: font,
	},
	cameraPreview: {
		flex: 1,
		justifyContent: "flex-end",
		alignItems: "center",
		alignSelf: "stretch",
	},
	cameraCapture: {
		flex: 0,
		backgroundColor: "#fff",
		borderRadius: 5,
		color: "#000",
		padding: 10,
		margin: 40
	},
	picker: {
		color: "#fff",
	},
	switchContainer: {
		alignItems: "flex-start",
	},
	textInput: {
		color: "#fff",
		height: 90,
		borderColor: yellow, 
		borderWidth: 1,
		textAlignVertical: "top",
		padding: 7,
		fontSize: baseFontSize,
	},
	checkboxLabel: {
		color: "#fff",
		fontSize: baseFontSize,
	},
	checkboxContainer: {
		marginTop: 10,
	},
	cameraButton: {
		height: 25 * PixelRatio.get(),
		width: 25 * PixelRatio.get(),
	},
	cameraBox: {
		backgroundColor: "#948181",
		height: 50* PixelRatio.get(),
		width: 50* PixelRatio.get(),
		margin: 1* PixelRatio.get(),
		alignItems: "center",
		justifyContent: "center",

	},
	cameraBoxSmall: {
		backgroundColor: "#948181",
		height: 30 * PixelRatio.get(),
		width: 30 * PixelRatio.get(),
		margin: 1* PixelRatio.get(),
	},

});

module.exports = styles;