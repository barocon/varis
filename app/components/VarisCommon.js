import React, { Component } from "react";
import { View, Text as TextRN, Switch as SwitchRN, TouchableHighlight, Image, ScrollView } from "react-native";
import ButtonRN from "react-native-button";
import styles from "varis/styles";
import { Navigation } from "react-native-navigation";
import CheckBoxRN from "react-native-checkbox";

export class ActionFrame extends Component {
	render() {
		return (
			<View style={[styles.actionFrame, this.props.style]}>
				{this.props.children}
			</View>
		);
	}
}

export class Button extends Component {
	render() {
		return (
			<View style={{alignItems:"center"}}>
				<ButtonRN
					containerStyle={styles.button}
					onPress={this.props.onPress}
					style={styles.buttonText}
				>
					<TextRN style={styles.buttonText}>{this.props.children}</TextRN>
				</ButtonRN>
			</View>
		);
	}
}

// TEMP style for navigation button
// Cant get it to work by importing from styles.js

export const nStyle = {
	navBarBackgroundColor: "#1E1C1A",
	navBarTextColor: "#FFD500",
	navBarTextFontFamily: "Oswald",
	navBarButtonColor: "#FFD500",
	statusBarTextColorScheme: "light",
};

export class NavButton extends Component {
	render() {
		const goto = this.props.goto;
		return (
			<View style={{alignItems:"center"}}>
				<ButtonRN
					containerStyle={[styles.button, this.props.style]}
					onPress={() => this.props.navigator.push(
						{
							screen: goto,
							navigatorStyle: nStyle,
							title: "",
							leftButtons: [{ id: "back" }, {	id: "sideMenu" } ],
							passProps: {latitude: this.props.latitude, longitude: this.props.longitude}, 
						},
					)}
					style={styles.buttonText}
				>
					<TextRN style={styles.buttonText}>{this.props.children}</TextRN>
				</ButtonRN>
			</View>
		);
	}
}

export class Switch extends Component {
	render() {
		return (
			<View style={styles.switchContainer}>
				<SwitchRN
					thumbTintColor="#FFD500"
					tintColor="#aaa"
					onValueChange={this.props.toggleSwitch}
					value={this.props.switchValue}
				/>
			</View>

		);
	}
}
/*

*/

export class Text extends Component {
	render() {
		return (
			<TextRN style={[styles.baseText, this.props.style]}>
				{this.props.children}
			</TextRN>
		);
	}
}

export class H2 extends Component {
	render() {
		return (
			<TextRN style={[styles.H2, this.props.style]}>
				{this.props.children}
			</TextRN>
		);
	}
}

export class H1 extends Component {
	render() {
		return (
			<TextRN style={[styles.H1, this.props.style]}>
				{this.props.children}
			</TextRN>
		);
	}
}

export class Container extends Component {
	render() {
		return (
			<View style={[styles.container, this.props.style]}>
				{this.props.children}
			</View>
		);
	}
}

export class Checkbox extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<CheckBoxRN
				label={this.props.label}
				checked={this.props.onPress.value}
				onChange={this.props.onPress}
				uncheckedImage={require("varis/img/checkbox-blank_yellow.png")}
				checkedImage={require("varis/img/checkbox-marked_yellow.png")}
				labelStyle={styles.checkboxLabel}
				containerStyle={styles.checkboxContainer}
				underlayColor="#FFD500"
				ref={component => this._root = component} {...this.props}
			/>
		);
	}
}

export class CameraButton extends Component {
	render() {
		return (
			<View style={[{ position: "absolute", height: 75, width: 75 }, this.props.style]}>
				<TouchableHighlight
					onPress={() =>
						this.props.navigator.showModal({ screen: "CameraPage" })
					}
				>
					<Image
						style={styles.cameraButton}
						source={require("varis/img/Camera-icon.png")}
						resizeMode="contain"
					/>
				</TouchableHighlight>
			</View>
		);
	}
}

export class CameraBox extends Component {
	render() {
		return (
			<TouchableHighlight
				onPress={() =>
					this.props.navigator.showModal({ screen: "CameraPage" })
				}
			>
				<View style={[styles.cameraBox, this.props.style]}>
					<Text>+</Text>
				</View>
			</TouchableHighlight>
		);
	}
}
export class CameraBoxSmall extends Component {
	render() {
		return (
			<TouchableHighlight

			>
				<View style={[styles.cameraBoxSmall, this.props.style]} />
			</TouchableHighlight>
		);
	}
}
