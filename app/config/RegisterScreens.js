import { Navigation } from "react-native-navigation";

import HomePage from "varis/app/screens/0_HomePage/HomePage";
import SafetyCheck from "varis/app/screens/1_SafetyCheck/SafetyCheck";
import LocationCheck from "varis/app/screens/2_LocationCheck/LocationCheck";
import CarPhotos from "varis/app/screens/3_CarPhotos/CarPhotos";
import OtherPhotos from "varis/app/screens/4_OtherPhotos/OtherPhotos";
import AccidentDetails from "varis/app/screens/5_AccidentDetails/AccidentDetails";
import AdditionalServices from "varis/app/screens/6_AdditionalServices/AdditionalServices";
import ConfirmDetails from "varis/app/screens/7_ConfirmDetails/ConfirmDetails";
import Confirmation from "varis/app/screens/8_Confirmation/Confirmation";
import CameraPage from "varis/app/screens/CameraPage/CameraPage";
import DrawerMenu from "varis/app/components/DrawerMenu";

// register all screens of the app (including internal ones)
export function registerScreens() {
	Navigation.registerComponent("HomePage", () => HomePage);
	Navigation.registerComponent("SafetyCheck", () => SafetyCheck);
	Navigation.registerComponent("LocationCheck", () => LocationCheck);
	Navigation.registerComponent("CarPhotos", () => CarPhotos);
	Navigation.registerComponent("OtherPhotos", () => OtherPhotos);
	Navigation.registerComponent("AccidentDetails", () => AccidentDetails);
	Navigation.registerComponent("AdditionalServices", () => AdditionalServices);
	Navigation.registerComponent("ConfirmDetails", () => ConfirmDetails);
	Navigation.registerComponent("Confirmation", () => Confirmation);
	Navigation.registerComponent("CameraPage", () => CameraPage);
	Navigation.registerComponent("DrawerMenu", () => DrawerMenu);
}