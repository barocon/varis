import React from "react";
import { AppRegistry } from "react-native";

import Camera from "react-native-camera";
import styles from "varis/styles";

import { Container, Button, } from "varis/app/components/VarisCommon";


export default class CameraPage extends React.Component {
	takePicture() {
		this.camera.capture()
			.then(data => console.log(data))
			.catch(err => console.error(err));
	}

	render() {
		return (
			<Container>
				<Camera
					ref={(cam) => {
						this.camera = cam;
					}}
					style={styles.cameraPreview}
					aspect={Camera.constants.Aspect.fill}
				>
					<Button
						navigator={this.props.navigator}
						onPress={(
							this.takePicture.bind(this),
							() =>
								this.props.navigator.dismissModal({
									animationType: "slide-down", // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
								})
						)}
					>
							CAPTURE
					</Button>
				</Camera>
			</Container>
		);
	}
}

AppRegistry.registerComponent("CameraPage", () => CameraPage);

