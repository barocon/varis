import React from "react";

import {ActionFrame, Container, NavButton, H1, Checkbox} from "varis/app/components/VarisCommon";


export default class AdditionalServices extends React.Component {
	constructor(props) {
		super(props);
	
		this.state = {
			towing: false,
			repair: false,
			rental: false,
			accomodation: false,
			pony: false,
			coffee: false,
		};
	}

	handleChange = (field, value) => {
		this.setState({...this.state, [field]: value});
	}
	
	render() {
		return (
			<Container>
				<ActionFrame>
					<H1>ADDITIONAL SERVICES</H1>
					<Checkbox
						label="Towing service"
						onPress={this.handleChange.bind(this, "towing")}
					/>
					
					<Checkbox
						label="Vehicle repair"
						onPress={this.handleChange.bind(this, "repair")}
					/>
					
					<Checkbox
						label="Car rental"
						onPress={this.handleChange.bind(this, "rental")}
					/>
					<Checkbox
						label="Accomodation"
						onPress={this.handleChange.bind(this, "accomodation")}
					/>
					<Checkbox
						label="Pony!"
						onPress={this.handleChange.bind(this, "pony")}
					/>
					<Checkbox
						label="Coffee and buns"
						onPress={this.handleChange.bind(this, "coffee")}
					/>
					<NavButton
						navigator = {this.props.navigator}
						goto = 'ConfirmDetails'
					>
               		 CONTINUE
					</NavButton>
				</ActionFrame>
			</Container>
		);
	}
}