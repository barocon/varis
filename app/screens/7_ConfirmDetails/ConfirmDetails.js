import React from "react";
import { View, ScrollView } from "react-native";


import {ActionFrame, Container, NavButton, Text, H1, CameraBoxSmall} from "varis/app/components/VarisCommon";


export default class ConfirmDetails extends React.Component {
	render() {
      
		return (
			<Container>
				<ScrollView>
					<ActionFrame>
						<H1>CONFIRM ACCIDENT INFORMATION</H1>
						<Text>
                    Name: Aku Ankka {"\n"}
                    Location: Paratiisintie 13, Ankkalinna 12345{"\n"}
                    1 vehicle involved{"\n"}
                    Police has already been notified{"\n"}

						</Text>
						<H1>ADDITIONAL SERVICES</H1>
						<Text>
                      - Towing {"\n"}
                      - Vehicle repair {"\n"}
                      - Car rental {"\n"}
                      - Accomodation {"\n"}
						</Text>
						<H1>PHOTOS</H1>
						<View style={{flex: 1, flexDirection: "row", flexWrap: "wrap", justifyContent: "center",}}>
							<CameraBoxSmall/>
							<CameraBoxSmall/>
							<CameraBoxSmall/>
							<CameraBoxSmall/>
							<CameraBoxSmall/>
							<CameraBoxSmall/>
							<CameraBoxSmall/>
							<CameraBoxSmall/>
						</View>
						<NavButton
							navigator = {this.props.navigator}
							goto = 'Confirmation'
						>
                    CONFIRM
						</NavButton>
					</ActionFrame>
				</ScrollView>      
			</Container>
		);
	}
}