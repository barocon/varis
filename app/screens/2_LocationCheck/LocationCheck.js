import React from "react";
import { View, StyleSheet } from "react-native";
import MapView from "react-native-maps";

import {ActionFrame, Container, NavButton, H1, Button} from "varis/app/components/VarisCommon";

const styles = StyleSheet.create({
	container: {

		height: "80%",
		flex: 1,
		justifyContent: "flex-end",
		alignItems: "center",
	},
	map: {
		...StyleSheet.absoluteFillObject,
	},
});

export default class LocationCheck extends React.Component {
	constructor(props) {
		super(props);
	
		this.state = {
			region: {
				latitude: this.props.latitude,
				longitude: this.props.longitude,
				latitudeDelta: 0.010,
				longitudeDelta: 0.011,
			},
		}
	}

	componentDidMount() {
		navigator.geolocation.getCurrentPosition(
			(position) => {
				this.setState({
					latitude: position.coords.latitude,
					longitude: position.coords.longitude,
					error: null,
				});
			},
			(error) => this.setState({ error: error.message }),
			{ enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 },
		);
	}

	render() {
		return (
			<Container>
				<ActionFrame>
					<View style ={styles.container}>
						<MapView
							style={styles.map}
							region={this.state.region}
							onRegionChange={this.onRegionChange}
						>
							<MapView.Marker 
								coordinate={{latitude: this.props.latitude, longitude: this.props.longitude}}
								title={"Your estimated location"}
							/>

							
						</MapView>
						
					</View>

					<H1>IS THIS YOUR LOCATION?</H1>
					<NavButton
						navigator = {this.props.navigator}
						goto = 'CarPhotos'            
					>
							CONFIRM
					</NavButton>
				</ActionFrame>
			</Container>
		);
	}
}
	