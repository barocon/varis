import React from "react";
import { View, StyleSheet, Image } from "react-native";
import {ActionFrame, Container, NavButton, Text, H1, CameraBox} from "varis/app/components/VarisCommon";


export default class OtherPhotos extends React.Component {
	render() {
      
		return (
			<Container>
				<ActionFrame>
					<H1>PLEASE TAKE PHOTOS OF THE ACCIDENT SITE</H1>
					<View style={{flex: 1, flexDirection: "row", flexWrap: "wrap", justifyContent: "center",}}>
						<CameraBox navigator = {this.props.navigator}/>
						<CameraBox navigator = {this.props.navigator}/>
						<CameraBox navigator = {this.props.navigator}/>
						<CameraBox navigator = {this.props.navigator}/>
						<CameraBox navigator = {this.props.navigator}/>
						<CameraBox navigator = {this.props.navigator}/>
					</View>

					<NavButton
						navigator = {this.props.navigator}
						goto = 'AccidentDetails'   
					>
                CONTINUE
					</NavButton>
				</ActionFrame>
			</Container>
		);
	}
}