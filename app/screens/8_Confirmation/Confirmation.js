import React from 'react';
import { View, StyleSheet, Image } from 'react-native';


import {ActionFrame, Container, Button, Text, H1} from 'varis/app/components/VarisCommon';


export default class Confirmation extends React.Component {
    render() {
      
      return (
        <Container>
            <ActionFrame>
                <H1>
                  YOUR REQUEST HAS BEEN SENT
                  {"\n\n"}
                  A TOW TRUCK HAS BEEN DISPATCHED AND MAY CONTACT YOU FOR ADDITIONAL INFORMATION
                  {"\n\n"}
                  YOU CAN SAFELY CLOSE THE APPLICATION NOW
                </H1>
            </ActionFrame>
        </Container>
      );
    }
  }