import React from "react";
import { View, StyleSheet, Image } from "react-native";

import {ActionFrame, Container, NavButton, Button, Text, H1, CameraButton} from "varis/app/components/VarisCommon";

export default class CarPhotos extends React.Component {
	render() {

		return (
			<Container>
				<ActionFrame>
					<H1>CAR PHOTOS SCREEN</H1>
					<Text>Please add photos of your car.</Text>
					<View style={{flex: 1, alignItems: "center", maxHeight: 400}}>
						<Image
							source={require("varis/img/car.png")}
							style={{ flex: 1, maxWidth: 350, maxHeight: 350, alignItems: "center", marginTop: 25 }}
							resizeMode="contain"
						/>
						{ /* Top */ }
						<CameraButton
							navigator = {this.props.navigator}
							style= {{top:0}}
						/>
						{ /* Right */ }
						<CameraButton
							navigator = {this.props.navigator}
							style= {{top:"45%", right:"15%"}}
						/>
						{ /* Left */ }
						<CameraButton
							navigator = {this.props.navigator}
							style= {{top:"45%", left:"15%"}}
						/>
						{ /* Bottom */ }
						<CameraButton
							navigator = {this.props.navigator}
							style= {{bottom:0}}
						/>
					</View>


					<NavButton
						navigator = {this.props.navigator}
						goto = 'OtherPhotos'     
					>
                  CONTINUE
					</NavButton>
				</ActionFrame>
			</Container>
		);
	}
}
  