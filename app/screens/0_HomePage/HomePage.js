import React from "react";
import { View, Image } from "react-native";

import {ActionFrame, Container, NavButton, Text, H1} from "varis/app/components/VarisCommon";

export default class HomePage extends React.Component {
	constructor(props) {
		super(props);
	
		this.state = {
			latitude: null,
			longitude: null,
			error: null,
		};
	}

	componentDidMount() {
		navigator.geolocation.getCurrentPosition(
			(position) => {
				this.setState({
				latitude: position.coords.latitude,
				longitude: position.coords.longitude,
				error: null,
				});
			},
			(error) => this.setState({ error: error.message }),
			//{ enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 },
		);
		}

	render() {
		//
      
		return (
			<Container>
				<ActionFrame>
					<View style={{alignItems: "center", flex: 1, maxHeight: "40%", }}>
						<Image
							source={require("varis/img/dualbirdlogoYV.png")}
							style={{ maxWidth: "99%", maxHeight: "99%", alignItems: "center",}}
							resizeMode="contain"
						/>
					</View>

					<Text></Text>
					<H1>VEHICLE ACCIDENT CLAIMS</H1>
					<Text>You can make a claim quickly and easily online right now.</Text>
					<Text>- Just 7 easy steps</Text>
					<Text>- Completed in under 10 minutes</Text>

					{this.state.latitude ? 
						<NavButton
						navigator = {this.props.navigator}
						goto = 'SafetyCheck'
						latitude = {this.state.latitude}
						longitude = {this.state.longitude}
						>
						REPORT AN ACCIDENT
						</NavButton>
						:
						<Text>Loading...</Text>
					}

                 

				</ActionFrame>
			</Container>
		);
	}
}