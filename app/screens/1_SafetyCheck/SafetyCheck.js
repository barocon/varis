import React from "react";
import { View } from "react-native";

import {ActionFrame, Container, NavButton, Button, Text, H1} from "varis/app/components/VarisCommon";

export default class SafetyCheck extends React.Component {
	render() {
		// 
		return (
			<Container>
				<ActionFrame>
					<H1>IS EVERYONE SAFE?</H1>
					<Button
						onPress={
							() => this.props.navigator.popToRoot({
								animated: true, // does the popToRoot have transition animation or does it happen immediately (optional)
								animationType: "fade", // 'fade' (for both) / 'slide-horizontal' (for android) does the popToRoot have different transition animation (optional)
							})
						}
					>
              NO, CALL 112
					</Button>

					<NavButton
						style = {{backgroundColor: "#08FF00"}}
						navigator = {this.props.navigator}
						goto = 'LocationCheck'
						latitude = {this.props.latitude}
						longitude = {this.props.longitude}
					>
              EVERYONE IS SAFE
					</NavButton>
				</ActionFrame>
			</Container>
		);
	}
}

  