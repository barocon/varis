import React from "react";
import { View, StyleSheet, Image, Picker, TextInput } from "react-native";
import styles from "varis/styles";


import {ActionFrame, Container, NavButton, Text, H1, H2, Switch} from "varis/app/components/VarisCommon";

export default class AccidentDetails extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			number: "1",
			switchValue: false,
			text: "Lorum ipsum"
		}
	}
	toggleSwitch() {
		this.setState({switchValue: !this.state.switchValue});
	}
	render() {
      
		return (
			<Container>
				<ActionFrame>
					<H1>ACCIDENT DETAILS</H1>
					<H2>How many vehicles were involved?</H2>
					<Picker
						style={styles.picker}
						selectedValue={this.state.number}
						onValueChange={(itemValue, itemIndex) => this.setState({number: itemValue})}>
						<Picker.Item label="1" value="1" />
						<Picker.Item label="2" value="2" />
						<Picker.Item label="3" value="3" />
						<Picker.Item label="4+" value="4+" />
					</Picker>
					<H2>Is there a police report of this accident?</H2>
					<Switch
						toggleSwitch = {this.toggleSwitch.bind(this)}
						switchValue = {this.state.switchValue}
					/>
					<H2>Briefly explain what happened</H2>
					<TextInput
						multiline = {true}
						numberOfLines = {1}
						style={styles.textInput}
						onChangeText={(text) => this.setState({text})}
						value={this.state.text}
					/>
					<NavButton
						navigator = {this.props.navigator}
						goto = 'AdditionalServices'
					>
                CONTINUE
					</NavButton>
				</ActionFrame>
			</Container>
		);
	}
}